﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Programmeren4TestStudenten
{
    class Week1
    {
        [Test]
        public void TestPower()
        {
            Assert.Throws<ArgumentException>(() => Power(2, -1));
            Assert.AreEqual(1, Power(2, 0));
            Assert.AreEqual(2, Power(2, 1));
            Assert.AreEqual(16, Power(2, 4));
            Assert.AreEqual(256, Power(2, 8));
            Assert.AreEqual(1024, Power(2, 10));
            Assert.AreEqual(1048576, Power(2, 20));

            Assert.AreEqual(1, Power(10, 0));
            Assert.AreEqual(10, Power(10, 1));
            Assert.AreEqual(100, Power(10, 2));
            Assert.AreEqual(1000, Power(10, 3));

            Assert.AreEqual(27, Power(3, 3));
        }

        // Implementeer een recursieve functie die 𝑚 tot de macht 𝑛 , 𝑛 ≥ 0 berekent.
        // Je mag  niet de methode Math.Pow() gebruiken!
        // De aanroep voor 2 tot de macht 8: Console.WriteLine(Power(2, 8));

        public int Power(int m, int n)
        {
            throw (new ArgumentException());
        }

        [Test]
        public void TestConvertToBinary()
        {
            Assert.Throws<ArgumentException>(() => ConvertToBinary(-11));
            Assert.AreEqual("0", ConvertToBinary(0));
            Assert.AreEqual("1", ConvertToBinary(1));
            Assert.AreEqual("111", ConvertToBinary(7));
            Assert.AreEqual("10110", ConvertToBinary(22));
            Assert.AreEqual("1111111", ConvertToBinary(127));
            Assert.AreEqual("10000000", ConvertToBinary(128));
        }

        // Implementeer een recursieve functie die een getal in binaire vorm omzet.
        // Let op: het returntype van de functie is string!
        // Voor n = 0 wordt "0" geretourneerd, voor n = 1, wordt "1" geretourneerd, 
        // voor n = 5, wordt "101" geretourneerd en voor n = 22  "10110".

        public string ConvertToBinary(int n)
        {
            throw (new ArgumentException());
        }

    [Test]
        public void TestSmartPower()
        {
            Assert.Throws<ArgumentException>(() => SmartPower(2, -2));
            Assert.AreEqual(1, SmartPower(2, 0));
            Assert.AreEqual(2, SmartPower(2, 1));
            Assert.AreEqual(16, SmartPower(2, 4));
            Assert.AreEqual(256, SmartPower(2, 8));
            Assert.AreEqual(1024, SmartPower(2, 10));
            Assert.AreEqual(1048576, SmartPower(2, 20));

            Assert.AreEqual(1, SmartPower(10, 0));
            Assert.AreEqual(10, SmartPower(10, 1));
            Assert.AreEqual(100, SmartPower(10, 2));
            Assert.AreEqual(1000, SmartPower(10, 3));

            Assert.AreEqual(27, SmartPower(3, 3));
        }

        // Implementeer een recursieve functie die op een slimme manier een getal 
        // tot een bepaalde macht verheft. De definities zijn:
        // m tot de macht n = (m * m) tot de macht n/2 als n > 0 en n is even
        //                  = m * (m tot de macht (n - 1)) als n > 0 en n is oneven
        // m tot de macht 0 = 1

            public int SmartPower(int m, int n)
        {
            throw (new ArgumentException());
        }

        [Test]
        public void TestTelManieren()
        {
            Assert.Throws<ArgumentException>(() => TelManieren(-2));
            Assert.AreEqual(1, TelManieren(0));
            Assert.AreEqual(1, TelManieren(1));
            Assert.AreEqual(2, TelManieren(2));
            Assert.AreEqual(3, TelManieren(3));
            Assert.AreEqual(5, TelManieren(4));
            Assert.AreEqual(8, TelManieren(5));
            Assert.AreEqual(13, TelManieren(6));
            Assert.AreEqual(21, TelManieren(7));
        }

        // Implementeer een recursieve functie die berekent op hoeveel manieren je een trap
        // van een bepaalde lengte kunt opgaan als je elke keer de keus hebt tussen 
        // een kleine (1 trede) en een grote (2 treden) stap.

        public int TelManieren(int treden)
        {
            throw (new ArgumentException());
        }

        [Test]
        public void TestIsOplosbaar()
        {
            Assert.Throws<ArgumentException>(() => IsOplosbaar(2, new int[] { 3, 1, 2, 3, 4, 0 }));
            Assert.Throws<ArgumentException>(() => IsOplosbaar(0, new int[] { 3, 0, 2, 3, 4, 0 }));
            Assert.Throws<ArgumentException>(() => IsOplosbaar(0, new int[] { 3, 1, 2, 3, 4, 2 }));
            Assert.Throws<ArgumentException>(() => IsOplosbaar(0, new int[] { 1 }));
            Assert.AreEqual(true, IsOplosbaar(0, new int[] { 0 }));
            Assert.AreEqual(true, IsOplosbaar(0, new int[] { 1, 0 }));
            Assert.AreEqual(false, IsOplosbaar(0, new int[] { 3, 1, 2, 3, 4, 0 }));
            Assert.AreEqual(true, IsOplosbaar(0, new int[] { 3, 6, 4, 1, 3, 4, 2, 5, 3, 0 }));
            Assert.AreEqual(true, IsOplosbaar(0, new int[] { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0 }));
            Assert.AreEqual(false, IsOplosbaar(0, new int[] { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 0 }));
        }

        // Implementeer de functie IsOplosbaar die uitrekent of een rij van gehele positieve 
        // getallen een oplossing heeft, zoals beschreven in opgave 5 van de opgaven uit week 1.
          
        public bool IsOplosbaar(int start, int[] vak)
        {
            throw (new ArgumentException());
        }
    }
}
