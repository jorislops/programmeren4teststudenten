﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Programmeren4TestStudenten
{
    class Week2
    {
        [Test]
        public void TestReverse()
        {
            Assert.AreEqual("", Reverse(""));
            Assert.AreEqual("a", Reverse("a"));
            Assert.AreEqual("bA", Reverse("Ab"));
            Assert.AreEqual("krov", Reverse("vork"));
            Assert.AreEqual("lepel", Reverse("lepel"));
            Assert.AreEqual("sem prehcs", Reverse("scherp mes"));
            Assert.AreEqual("Dit is een fiets.", Reverse(".steif nee si tiD"));
        }

        // Implementeer een recursieve functie die het omgekeerde van een string oplevert.

        public string Reverse(string text)
        {
            throw (new ArgumentException());
        }

        [Test]
        public void TestIsPalindrome()
        {
            Assert.AreEqual(true, IsPalindrome(""));
            Assert.AreEqual(true, IsPalindrome("a"));
            Assert.AreEqual(false, IsPalindrome("ab"));
            Assert.AreEqual(true, IsPalindrome("parterretrap"));
            Assert.AreEqual(true, IsPalindrome("meetsysteem"));
            Assert.AreEqual(true, IsPalindrome("Nelli plaatst op ene parterretrap ene pot staalpillen"));
            Assert.AreEqual(false, IsPalindrome("Nelli plaatst op ene parterretrap ee pot staalpillen"));
            Assert.AreEqual(true, IsPalindrome("No lemon, no melon"));
        }

        // Implementeer een recursieve functie die bepaalt of een string een palindroom is. 
        // Een palindroom is een string die van voren naar achteren hetzelfde is als van 
        // achteren naar voren

        public bool IsPalindrome(string text)
        {
            throw (new ArgumentException());
        }

        [Test]
        public void TestIterSum()
        {
            Assert.AreEqual(0, IterSum(new int[] { }, 0, 0));
            Assert.AreEqual(0, IterSum(new int[] { 1, 2, 3, 4 }, 3, 0));
            Assert.AreEqual(5, IterSum(new int[] { 5 }, 0, 1));
            Assert.AreEqual(10, IterSum(new int[] { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 }, 0, 10));
            Assert.AreEqual(0, IterSum(new int[] { 1, -1, 1, -1, 1, -1, 1, -1, 1, -1 }, 0, 10));
        }

        // Schrijf een functie die de som van een array in een loop berekent.

        public int IterSum(int[] elems, int start, int length)
        {
            throw (new ArgumentException());
        }

        [Test]
        public void TestRecSum()
        {
            Assert.AreEqual(0, RecSum(new int[] { }, 0, 0));
            Assert.AreEqual(0, RecSum(new int[] { 1, 2, 3, 4 }, 3, 0));
            Assert.AreEqual(5, RecSum(new int[] { 5 }, 0, 1));
            Assert.AreEqual(10, RecSum(new int[] { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 }, 0, 10));
            Assert.AreEqual(0, RecSum(new int[] { 1, -1, 1, -1, 1, -1, 1, -1, 1, -1 }, 0, 10));
        }

        // Implementeer een functie die de som van een array recursief berekent.

        public int RecSum(int[] elems, int start, int length)
        {
            throw (new ArgumentException());
        }

        [Test]
        public void TestIntList()
        {
            IntList list = new IntList(6, null);
            list = new IntList(5, list);
            list = new IntList(3, list);
            list = new IntList(3, list);
            list = new IntList(1, list);
            Assert.AreEqual("1 3 3 5 6", list.ToString());
            list = list.Add(5);
            list = list.Add(7);
            list = list.Add(0);
            Assert.AreEqual("0 1 3 3 5 5 6 7", list.ToString());
            Assert.AreEqual(true, Math.Abs(3.75 - list.Average()) < 0.00001);
            list = list.Add(-3);
            list = list.Add(-1);
            Assert.AreEqual("-3 -1 0 1 3 3 5 5 6 7", list.ToString());
            Assert.AreEqual(true, Math.Abs(2.6 - list.Average()) < 0.00001);
        }

        class IntList
        {
            private int head;
            private IntList tail;

            // Maak bij deze class een eenvoudige constructor, waarmee je een lijst kunt maken 
            // die de volgende elementen in diezelfde volgorde bevat: 1, 3, 5, 5, 6. 
            // Let daarbij op dat je afsluit met tail = null.

            public IntList(int hd, IntList tl)
            {
                head = hd;
                tail = tl;
            }

            // Schrijf een methode die het aantal elementen in de lijst telt

            public int Length()
            {
                throw (new ArgumentException());
            }

            // Schrijf een methode die de som van de lementen van de lijst berekent

            public int Sum()
            {
                throw (new ArgumentException());
            }

            // Schrijf een methode die het gemiddelde van de elementen van de lijst berekent

            public float Average()
            {
                throw (new ArgumentException());
            }

            // Maak een methode Add van de class Intlist die ervoor zorgt dat een element 
            // van het type int wordt toegevoegd aan de lijst op een zodanige positie dat 
            // de lijst van klein naar groot gesorteerd blijft.
            // Bij het toevoegen van een element mag je er dus van uitgaan dat de lijst gesorteerd is. 
            // Er mogen gelijke elementen in de lijst voorkomen

            public IntList Add(int i)
            {
                throw (new ArgumentException());
            }

            // Om te kunnen controleren of je het goed doet, maak je ook een methode ToString 
            // die de elementen van de lijst van voren naar achteren in een string zetten

            public override string ToString()
            {
                throw (new ArgumentException());
            }
        }
    }
}
