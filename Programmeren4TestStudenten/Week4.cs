﻿using System;
using NUnit.Framework;

// Programmeren 4 week 4
//
// Onderstaand vind je twee onderdelen: List en BinTree met
// respectievelijk "TestList" en "TestBinTree" als bijbehorende tests.
// Deze klassen zijn in de les behandeld.
// Beide klassen zijn abstracte klassen met twee concrete subklassen.
// Overal waar nu "throw new NotImplementedException();" staat,
// moet je de goede code invullen.
// De betekenis van de methoden spreken voor zichzelf.
// Als je dat gedaan hebt, dan krijg je positieve testresultaten
// op "TestList"en "TestBinTree".

namespace Programmeren4TestStudenten
{
    class Week4
    {
        [Test]
        public void TestList()
        {
            List list = new Nil();
            Assert.AreEqual(true, list.IsEmpty);
            Assert.AreEqual(0, list.Count);
            Assert.AreEqual(0, list.Sum);
            Assert.AreEqual(true, Math.Abs(float.MaxValue - list.Average) < 0.00001);
            Assert.AreEqual(string.Empty, list.ToString());
            Assert.AreEqual(string.Empty, list.ReverseString());
            list = new Cons(1, new Cons(3, new Cons(3, new Cons(5, new Cons(6, list)))));
            Assert.AreEqual("1 3 3 5 6", list.ToString());
            list = list.Add(5).Add(7).Add(0);
            Assert.AreEqual("0 1 3 3 5 5 6 7", list.ToString());
            Assert.AreEqual("7 6 5 5 3 3 1 0", list.ReverseString());
            Assert.AreEqual(8, list.Count);
            Assert.AreEqual(30, list.Sum);
            Assert.AreEqual(true, Math.Abs(3.75 - list.Average) < 0.00001);
            list = list.Add(-3).Add(-1);
            Assert.AreEqual("-3 -1 0 1 3 3 5 5 6 7", list.ToString());
            Assert.AreEqual(true, Math.Abs(2.6 - list.Average) < 0.00001);
            Assert.AreEqual(true, Math.Abs(float.MaxValue - new Nil().Average) < 0.00001);
        }

        abstract class List
        {
            // Met de methode Add wordt een nieuw getal in de lijst ingevoerd.
            // De nummers in de lijst blijven daarbij op violgorde van grootte.
            // In de lijst mogen nummers meerdere keren voorkomen
            public abstract List Add(int i);
            // IsEmpty geeft aan of een lijst leeg is.
            public abstract bool IsEmpty { get; }
            // Count geeft het aantal getallen in de lijst aan.
            public abstract int Count { get; }
            // Sum geeft de som van alle getallen in de lijst aan.
            public abstract int Sum { get; }
            // Average is het gemiddelde van de getallen in de lijst. 
            // Let op dat het resultaattype een float is.
            public abstract float Average { get; }
            // Met ToString verkrijg je een stringrepresentatie van de getallen in de lijst.
            public abstract override string ToString();
            // Met ReverseString verkrijg je ook een stringrepresentatie van de getallen in de lijst,
            // maar dan in omgekeerde volgorde.
            public abstract string ReverseString();
        }

        class Nil : List
        {
            public Nil() { }

            public override List Add(int i)
            {
                throw new NotImplementedException();
            }

            public override bool IsEmpty
            {
                get { return true; }
            }

            public override int Count
            {
                get { return 0; }
            }

            public override int Sum
            {
                get { return 0; }
            }

            public override float Average
            {
                get { return float.MaxValue; }
            }

            public override string ToString()
            {
                throw new NotImplementedException();
            }

            public override string ReverseString()
            {
                throw new NotImplementedException();
            }
        }

        class Cons : List
        {
            public int head { get; }
            public List tail { get; private set; }

            public Cons(int hd, List tl)
            {
                throw new NotImplementedException();
            }

            public override List Add(int i)
            {
                throw new NotImplementedException();
            }

            public override bool IsEmpty
            {
                get { throw new NotImplementedException(); }
            }

            public override int Count
            {
                get { throw new NotImplementedException(); }
            }

            public override int Sum
            {
                get { throw new NotImplementedException(); }
            }

            public override float Average
            {
                get { throw new NotImplementedException(); }
            }

            public override string ToString()
            {
                throw new NotImplementedException();
            }

            public override string ReverseString()
            {
                throw new NotImplementedException();
            }
        }

        [Test]
        public void TestBinTree()
        {
            BinTree tree = new NilTree();
            Assert.AreEqual(true, tree.IsEmpty);
            Assert.AreEqual(false, tree.IsPresent(1));
            Assert.AreEqual(-1, tree.Depth);
            Assert.AreEqual(0, tree.Count);
            Assert.AreEqual(0, tree.Sum);
            Assert.AreEqual(true, Math.Abs(float.MaxValue - tree.Average) < 0.00001);
            Assert.AreEqual(string.Empty, tree.ToString());
            Assert.AreEqual(string.Empty, tree.ReverseString());
            tree = tree.Add(5).Add(6).Add(4).Add(2).Add(7).Add(3).Add(7).Add(8);
            Assert.AreEqual(false, tree.IsEmpty);
            Assert.AreEqual(false, tree.IsPresent(1));
            Assert.AreEqual(true, tree.IsPresent(2));
            Assert.AreEqual(7, tree.Count);
            Assert.AreEqual(35, tree.Sum);
            Assert.AreEqual(true, Math.Abs(5.0 - tree.Average) < 0.00001);
            Assert.AreEqual(3, tree.Depth);
            Assert.AreEqual(" 2 3 4 5 6 7 8", tree.ToString());
            Assert.AreEqual("8 7 6 5 4 3 2 ", tree.ReverseString());
        }

        abstract class BinTree
        {
            // Met de methode Add wordt een nieuw getal in de tree ingevoerd.
            // De nummers in de tree blijven daarbij op volgorde, dat wil zeggen dat
            // de linkernode alleen getallen bevat die kleiner zijn en de rechternode
            // alleen getallen die groter zijn dan het getal in de Node.
            // In de tree komen nummers hooguit 1 keer voor. Het invoeren van een getal\
            // dat al in de tree zit, wordt genegeerd.
            public abstract BinTree Add(int i);
            // IsEmpty geeft aan of een tree leeg is.
            public abstract bool IsEmpty { get; }
            // IsEmpty inspecteert of i in de tree zit opgeslagen
            public abstract bool IsPresent(int i);
            // Depth geeft de diepte van de tree aan. Depth wordt als volgt bepaald:
            // Depth van een NilTree is gelijk aan -1;
            // Depth van een NodeTree is gelijk aan het maximum van de Depths van zijn linker-
            // en rechternode plus 1.
            public abstract int Depth { get; }
            // Count geeft het aantal getallen in de tree aan.
            public abstract int Count { get; }
            // Sum geeft de som van alle getallen in de tree aan.
            public abstract int Sum { get; }
            // Average is het gemiddelde van de getallen in de tree. 
            // Let op dat het resultaattype een float is.
            public abstract float Average { get; }
            // Met ToString verkrijg je een stringrepresentatie van de getallen in de tree.
            public abstract override string ToString();
            // Met ReverseString verkrijg je ook een stringrepresentatie van de getallen in de tree,
            // maar dan in omgekeerde volgorde.
            public abstract string ReverseString();
        }

        class NilTree : BinTree
        {
            public NilTree() { }

            public override BinTree Add(int i)
            {
                throw new NotImplementedException();
            }

            public override bool IsEmpty
            {
                get { return true; }
            }

            public override bool IsPresent(int i)
            {
                return false;
            }

            public override int Depth
            {
                get { return -1; }
            }

            public override int Count
            {
                get { return 0; }
            }

            public override int Sum
            {
                get { return 0; }
            }

            public override float Average
            {
                get { return float.MaxValue; }
            }

            public override string ToString()
            {
                throw new NotImplementedException();
            }

            public override string ReverseString()
            {
                throw new NotImplementedException();
            }
        }

        class NodeTree : BinTree
        {
            private int value;
            private BinTree left;
            private BinTree right;

            public NodeTree(int vl, BinTree lft, BinTree rght)
            {
                new NotImplementedException();
            }

            public override BinTree Add(int i)
            {
                throw new NotImplementedException();
            }

            public override bool IsEmpty
            {
                get { throw new NotImplementedException(); }
            }

            public override bool IsPresent(int i)
            {
                throw new NotImplementedException();
            }

            public override int Depth
            {
                get { throw new NotImplementedException(); }
            }

            public override int Count
            {
                get { throw new NotImplementedException(); }
            }

            public override int Sum
            {
                get { throw new NotImplementedException(); }
            }

            public override float Average
            {
                get { throw new NotImplementedException(); }
            }

            public override string ToString()
            {
                throw new NotImplementedException();
            }

            public override string ReverseString()
            {
                throw new NotImplementedException();
            }
        }
    }
}
